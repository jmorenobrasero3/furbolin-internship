package net.aspanc.furbolin_internship.models;

import jakarta.persistence.*;
import lombok.Data;
import net.aspanc.furbolin_internship.utils.enums.MatchType;
import java.util.Map;

@Entity(name = "matches")
@Data
public class MatchModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type", nullable = false, columnDefinition = "ENUM('GROUP','ROUND16','QUARTER','SEMI','FINAL')")
    @Enumerated(EnumType.STRING)
    private MatchType type;

    @ManyToOne
    @JoinColumn(name = "winner_id", referencedColumnName = "id")
    private TeamModel winner;

    @ManyToOne
    @JoinColumn(name = "tournament_id", nullable = false)
    private TournamentModel tournament;

    @ElementCollection
    @MapKeyJoinColumn(name = "team_id")
    private Map<TeamModel, Integer> scores;

}
