package net.aspanc.furbolin_internship.models;

import jakarta.persistence.*;
import lombok.Data;
import net.aspanc.furbolin_internship.utils.enums.PlayerRole;

import java.util.List;


@Entity(name = "players")
@Data
public class PlayerModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(nullable = false)
  private String name;

  @Column(columnDefinition = "ENUM('STRIKER','DEFENDER','VERSATILE')")
  @Enumerated
  private PlayerRole role;

  @ManyToMany(mappedBy = "players")
  private List<TeamModel> teamModelList;

}
