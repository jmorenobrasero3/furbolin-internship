package net.aspanc.furbolin_internship.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import jakarta.persistence.EnumType;

import lombok.Data;
import net.aspanc.furbolin_internship.utils.enums.UserRole;

@Data
@Entity(name = "users")
public class UserModel {
    @Id
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "role", columnDefinition = "ENUM('USER','ADMIN')")
    @Enumerated(EnumType.STRING)
    private UserRole role;
}
