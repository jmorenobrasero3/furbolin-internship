package net.aspanc.furbolin_internship.models;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity(name = "tournaments")
@Data
public class TournamentModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "tournament")
    private List<MatchModel> matches;

    @OneToMany(mappedBy = "tournament")
    private List<TeamModel> teams;
}
