package net.aspanc.furbolin_internship.models.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import net.aspanc.furbolin_internship.models.UserModel;

public interface UserDao extends JpaRepository<UserModel, String> {
}
