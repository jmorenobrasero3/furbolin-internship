package net.aspanc.furbolin_internship.models.daos;

import net.aspanc.furbolin_internship.models.TeamModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamDao extends JpaRepository<TeamModel, Long> {

    List<TeamModel> findByNameContainingIgnoreCase(String name);
}
