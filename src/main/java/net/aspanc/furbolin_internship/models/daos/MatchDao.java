package net.aspanc.furbolin_internship.models.daos;
import net.aspanc.furbolin_internship.models.MatchModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchDao extends JpaRepository<MatchModel, Long> {
}
