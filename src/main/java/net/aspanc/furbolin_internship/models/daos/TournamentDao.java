package net.aspanc.furbolin_internship.models.daos;
import net.aspanc.furbolin_internship.models.TournamentModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TournamentDao extends JpaRepository<TournamentModel, Long> {
}