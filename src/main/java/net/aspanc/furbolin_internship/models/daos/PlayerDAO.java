package net.aspanc.furbolin_internship.models.daos;

import net.aspanc.furbolin_internship.models.PlayerModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlayerDAO extends JpaRepository<PlayerModel, Long> {
     Optional<PlayerModel> findByNameContainingIgnoreCase(String name);
}
