package net.aspanc.furbolin_internship.models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity(name = "teams")
public class TeamModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "winner")
    private List<MatchModel> matches;

    @ManyToOne
    @JoinColumn(name = "tournament_id", nullable = false)
    private TournamentModel tournament;

    @ManyToMany
    @JoinTable(name = "players_teams", joinColumns = @JoinColumn(name = "team_id"), inverseJoinColumns = @JoinColumn(name = "player_id"))
    private List<PlayerModel> players;
}
