package net.aspanc.furbolin_internship.config;


import jakarta.annotation.Resource;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

  @Resource
  private UserDetailsService userDetailsService;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http.authenticationProvider(authenticationProvider());
    http.csrf().disable();
    http.authorizeHttpRequests((requests) -> requests.requestMatchers("/", "/login", "css/**", "img/**", "js/**").permitAll()

        .requestMatchers("/admin/**").hasRole("ADMIN")
        .anyRequest().authenticated()

      )
      .formLogin()
      /*          .loginPage("/login").defaultSuccessUrl("/users/")*/
      .failureUrl("/login?error=true")
      .and()
      .logout()
      .logoutUrl("/logout")
      /* .logoutSuccessUrl("/login")*/
      .invalidateHttpSession(true).deleteCookies("JSESSIONID")
    ;
    return http.build();
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(passwordEncoder());

    return authProvider;
  }

  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfiguration) throws Exception {
    return authConfiguration.getAuthenticationManager();
  }


  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }


}
