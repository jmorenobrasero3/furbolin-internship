package net.aspanc.furbolin_internship.services;


import net.aspanc.furbolin_internship.models.PlayerModel;

import java.util.List;
import java.util.Optional;

public interface PlayerService {

  Optional<PlayerModel> findById(long id);

  List<PlayerModel> findAll();

  PlayerModel save(PlayerModel playerModel);

  void delete(long id);
  Optional<PlayerModel> findByNameContainingIgnoreCase(String name);

}
