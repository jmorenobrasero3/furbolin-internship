package net.aspanc.furbolin_internship.services;

import net.aspanc.furbolin_internship.models.TournamentModel;
import java.util.List;
import java.util.Optional;

public interface TournamentService {

    Optional<TournamentModel> findById(long id);

    List<TournamentModel> findAll();

    TournamentModel save(TournamentModel tournamentModel);

    void delete(long id);

}
