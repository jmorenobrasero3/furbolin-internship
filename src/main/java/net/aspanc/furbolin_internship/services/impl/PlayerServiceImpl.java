package net.aspanc.furbolin_internship.services.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.models.PlayerModel;
import net.aspanc.furbolin_internship.models.daos.PlayerDAO;
import net.aspanc.furbolin_internship.services.PlayerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Resource
    private PlayerDAO playerDAO;

    @Override
    public Optional<PlayerModel> findById(long id) {
        return playerDAO.findById(id);
    }

    @Override
    public List<PlayerModel> findAll() {

        return playerDAO.findAll();
    }

    @Override
    public PlayerModel save(PlayerModel playerModel) {

        return playerDAO.save(playerModel);

    }

    @Override
    public void delete(long id) {
        playerDAO.deleteById(id);
    }

    @Override
    public Optional<PlayerModel> findByNameContainingIgnoreCase(String name) {
        return playerDAO.findByNameContainingIgnoreCase(name);
    }

}
