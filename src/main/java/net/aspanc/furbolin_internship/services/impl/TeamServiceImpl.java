package net.aspanc.furbolin_internship.services.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.models.TeamModel;
import net.aspanc.furbolin_internship.models.daos.TeamDao;
import net.aspanc.furbolin_internship.services.TeamService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {

    @Resource
    private TeamDao teamDao;

    @Override
    public Optional<TeamModel> findById(long id) {
        return teamDao.findById(id);
    }

    @Override
    public List<TeamModel> findAll() {
        return teamDao.findAll();
    }

    @Override
    public TeamModel save(TeamModel teamModel) {
        return teamDao.save(teamModel);
    }

    @Override
    public void delete(long id) {
        teamDao.deleteById(id);
    }

    @Override
    public List<TeamModel> findByName(String name) {
        return teamDao.findByNameContainingIgnoreCase(name);
    }
}
