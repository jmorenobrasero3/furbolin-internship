package net.aspanc.furbolin_internship.services.impl;

import net.aspanc.furbolin_internship.config.WebSecurityConfig;
import net.aspanc.furbolin_internship.models.daos.UserDao;
import net.aspanc.furbolin_internship.models.UserModel;
import net.aspanc.furbolin_internship.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    UserDao userDao;
  private WebSecurityConfig webSecurityConfig;

    @Autowired
    public UserServiceImpl(UserDao userDao,WebSecurityConfig webSecurityConfig) {
        this.userDao = userDao;
      this.webSecurityConfig=webSecurityConfig;
    }

    @Override
    public Optional<UserModel> findById(String username) {
        return userDao.findById(username);
    }

    @Override
    public UserModel save(UserModel user) {
      user.setPassword(webSecurityConfig.passwordEncoder().encode(user.getPassword()));
        return userDao.save(user);
    }

    @Override
    public void delete(UserModel user) {
        userDao.delete(user);
    }

    @Override
    public List<UserModel> findAll() {
        return userDao.findAll();
    }

  @Override
  public Optional<UserDetails> getLoggedUser() {
    return (Optional<UserDetails>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
