package net.aspanc.furbolin_internship.services.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.models.TournamentModel;
import net.aspanc.furbolin_internship.models.daos.TournamentDao;
import net.aspanc.furbolin_internship.services.TournamentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TournamentServiceImpl implements TournamentService {

    @Resource
    private TournamentDao tournamentDao;

    @Override
    public Optional<TournamentModel> findById(long id) {
        return tournamentDao.findById(id);
    }

    @Override
    public List<TournamentModel> findAll() {
        return tournamentDao.findAll();
    }

    @Override
    public TournamentModel save(TournamentModel tournamentModel) {
        return tournamentDao.save(tournamentModel);
    }

    @Override
    public void delete(long id) {
        tournamentDao.deleteById(id);
    }
}
