package net.aspanc.furbolin_internship.services.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.models.MatchModel;
import net.aspanc.furbolin_internship.models.daos.MatchDao;
import net.aspanc.furbolin_internship.services.MatchService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MatchServiceImpl implements MatchService {

    @Resource
    private MatchDao matchDao;

    @Override
    public Optional<MatchModel> findById(long id) {
        return matchDao.findById(id);
    }

    @Override
    public List<MatchModel> findAll() {
        return matchDao.findAll();
    }

    @Override
    public void delete(long id) {
        matchDao.deleteById(id);
    }

    @Override
    public MatchModel save(MatchModel matchModel) {
        return matchDao.save(matchModel);
    }

}
