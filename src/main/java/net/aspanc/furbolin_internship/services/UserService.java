package net.aspanc.furbolin_internship.services;

import net.aspanc.furbolin_internship.models.UserModel;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;


public interface UserService {
  Optional<UserModel> findById(String username);

  UserModel save(UserModel user);

  void delete(UserModel user);

  List<UserModel> findAll();

  public Optional<UserDetails> getLoggedUser();

}
