package net.aspanc.furbolin_internship.services;

import net.aspanc.furbolin_internship.models.TeamModel;

import java.util.List;
import java.util.Optional;

public interface TeamService {

    Optional<TeamModel> findById(long id);

    List<TeamModel> findAll();

    TeamModel save(TeamModel teamModel);

    void delete(long id);

    List<TeamModel> findByName(String name);
}
