package net.aspanc.furbolin_internship.services;

import net.aspanc.furbolin_internship.models.MatchModel;

import java.util.List;
import java.util.Optional;

public interface MatchService {

    Optional<MatchModel> findById(long id);

    List<MatchModel> findAll();

    void delete(long id);

    MatchModel save(MatchModel matchModel);

}
