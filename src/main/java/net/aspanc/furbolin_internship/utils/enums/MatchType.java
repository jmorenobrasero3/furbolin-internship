package net.aspanc.furbolin_internship.utils.enums;

public enum MatchType {
    GROUP, ROUND16, QUARTER, SEMI, FINAL
}
