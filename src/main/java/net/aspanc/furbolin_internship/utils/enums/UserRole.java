package net.aspanc.furbolin_internship.utils.enums;

public enum UserRole {
    ADMIN, USER
}
