package net.aspanc.furbolin_internship.facade;

import net.aspanc.furbolin_internship.dtos.UserDto;
import net.aspanc.furbolin_internship.models.UserModel;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserFacade {

    Optional<UserDto> findById(String username);

    UserDto save(UserDto user);

    void delete(UserDto user);

    List<UserDto> findAll();

    public Optional<UserDetails> getLoggedUser();
}
