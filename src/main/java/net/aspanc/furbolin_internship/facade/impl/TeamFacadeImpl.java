package net.aspanc.furbolin_internship.facade.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.dtos.TeamDto;
import net.aspanc.furbolin_internship.facade.TeamFacade;
import net.aspanc.furbolin_internship.facade.converters.TeamDtoToTeamModelConverter;
import net.aspanc.furbolin_internship.facade.converters.TeamModelToTeamDtoConverter;
import net.aspanc.furbolin_internship.models.TeamModel;
import net.aspanc.furbolin_internship.services.TeamService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TeamFacadeImpl implements TeamFacade {

    @Resource
    private TeamService teamService;

    @Resource
    private TeamDtoToTeamModelConverter teamDtoToTeamModelConverter;

    @Resource
    private TeamModelToTeamDtoConverter teamModelToTeamDtoConverter;


    @Override
    public Optional<TeamDto> findById(long id) {
        Optional<TeamModel> teamModel = teamService.findById(id);
        return teamModel.map(teamModelToTeamDtoConverter::convert);
    }

    @Override
    public List<TeamDto> findAll() {
        return teamService.findAll().stream().map(teamModelToTeamDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    public TeamDto save(TeamDto teamDto) {
        return teamModelToTeamDtoConverter.convert(teamService.save(teamDtoToTeamModelConverter.convert(teamDto)));
    }

    @Override
    public void delete(long id) {
        teamService.delete(id);
    }

    @Override
    public List<TeamDto> findByName(String name) {
        return teamService.findByName(name).stream().map(teamModelToTeamDtoConverter::convert).collect(Collectors.toList());
    }
}
