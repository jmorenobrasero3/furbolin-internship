package net.aspanc.furbolin_internship.facade.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.dtos.MatchDto;
import net.aspanc.furbolin_internship.facade.MatchFacade;
import net.aspanc.furbolin_internship.facade.converters.MatchDtoToMatchModelConverter;
import net.aspanc.furbolin_internship.facade.converters.MatchModelToMatchDtoConverter;
import net.aspanc.furbolin_internship.models.MatchModel;
import net.aspanc.furbolin_internship.services.MatchService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class MatchFacadeImpl implements MatchFacade {

    @Resource
    private MatchService matchService;

    @Resource
    private MatchModelToMatchDtoConverter matchModelToMatchDtoConverter;

    @Resource
    private MatchDtoToMatchModelConverter matchDtoToMatchModelConverter;

    @Override
    public Optional<MatchDto> findById(long id) {
        return matchService.findById(id).map(matchModelToMatchDtoConverter::convert);
    }

    @Override
    public List<MatchDto> findAll() {
        return matchService.findAll().stream().map(matchModelToMatchDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    public void delete(long id) {
        matchService.delete(id);
    }

    @Override
    public MatchDto save(MatchDto matchModel) {
        return matchModelToMatchDtoConverter.convert(matchService.save(matchDtoToMatchModelConverter.convert(matchModel)));
    }
}
