package net.aspanc.furbolin_internship.facade.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.dtos.TournamentDto;
import net.aspanc.furbolin_internship.facade.TournamentFacade;
import net.aspanc.furbolin_internship.facade.converters.TournamentDtoToTournamentModelConverter;
import net.aspanc.furbolin_internship.facade.converters.TournamentModelToTournamentDtoConverter;
import net.aspanc.furbolin_internship.models.TournamentModel;
import net.aspanc.furbolin_internship.services.impl.TournamentServiceImpl;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TournamentFacadeImpl implements TournamentFacade {

    @Resource
    private TournamentServiceImpl tournamentService;

    @Resource
    private TournamentModelToTournamentDtoConverter modelToTournamentDtoConverter;

    @Resource
    private TournamentDtoToTournamentModelConverter dtoToTournamentModelConverter;

    @Override
    public List<TournamentDto> findAll() {
        return tournamentService.findAll().stream().map(modelToTournamentDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    public Optional<TournamentDto> findById(Long id) {
        Optional<TournamentModel> tournamentFound = tournamentService.findById(id);
        return tournamentFound.map(modelToTournamentDtoConverter::convert);
    }

    @Override
    public void delete(Long id) {
        tournamentService.delete(id);
    }

    @Override
    public TournamentDto save(TournamentDto tournamentDto) {
        return modelToTournamentDtoConverter.convert(tournamentService.save(dtoToTournamentModelConverter.convert(tournamentDto)));
    }

}
