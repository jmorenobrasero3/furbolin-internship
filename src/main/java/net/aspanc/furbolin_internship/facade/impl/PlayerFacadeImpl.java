package net.aspanc.furbolin_internship.facade.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.dtos.PlayerDto;
import net.aspanc.furbolin_internship.facade.PlayerFacade;
import net.aspanc.furbolin_internship.facade.converters.PlayerDtoToPlayerModelConverter;
import net.aspanc.furbolin_internship.facade.converters.PlayerModelToPlayerDtoConverter;
import net.aspanc.furbolin_internship.services.PlayerService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PlayerFacadeImpl implements PlayerFacade {

    @Resource
    private PlayerService playerService;

    @Resource
    private PlayerDtoToPlayerModelConverter playerDtoToPlayerModelConverter;

    @Resource
    private PlayerModelToPlayerDtoConverter playerModelToPlayerDtoConverter;

    @Override
    public List<PlayerDto> findAll() {

        return playerService
                .findAll()
                .stream()
                .map(playerModelToPlayerDtoConverter::convert)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<PlayerDto> findById(long id) {

        return playerService
                .findById(id)
                .map(playerModelToPlayerDtoConverter::convert);

    }

    @Override
    public List<PlayerDto> findByName(String name) {

        return playerService
                .findByNameContainingIgnoreCase(name)
                .stream()
                .map(playerModelToPlayerDtoConverter::convert)
                .collect(Collectors.toList());

    }

    @Override
    public void delete(long id) {
        playerService.delete(id);
    }

    @Override
    public PlayerDto save(PlayerDto playerDto) {

       return playerModelToPlayerDtoConverter
               .convert(playerService
                       .save(playerDtoToPlayerModelConverter
                               .convert(playerDto)));

    }

}
