package net.aspanc.furbolin_internship.facade.impl;

import jakarta.annotation.Resource;
import net.aspanc.furbolin_internship.dtos.UserDto;
import net.aspanc.furbolin_internship.facade.UserFacade;
import net.aspanc.furbolin_internship.facade.converters.TeamDtoToTeamModelConverter;
import net.aspanc.furbolin_internship.facade.converters.TeamModelToTeamDtoConverter;
import net.aspanc.furbolin_internship.facade.converters.UserDtoToUserModelConverter;
import net.aspanc.furbolin_internship.facade.converters.UserModelToUserDtoConverter;
import net.aspanc.furbolin_internship.models.UserModel;
import net.aspanc.furbolin_internship.services.TeamService;
import net.aspanc.furbolin_internship.services.UserService;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserFacadeImpl implements UserFacade {

    @Resource
    private UserService userService;

    @Resource
    private UserDtoToUserModelConverter userDtoToUserModelConverter;

    @Resource
    private UserModelToUserDtoConverter userModelToUserDtoConverter;
    @Override
    public Optional<UserDto> findById(String username) {
        return userService.findById(username).map(userModelToUserDtoConverter::convert);
    }

    @Override
    public UserDto save(UserDto user) {
        return userModelToUserDtoConverter.convert(userService.save(userDtoToUserModelConverter.convert(user)));
    }

    @Override
    public void delete(UserDto user) {
        userService.delete(userDtoToUserModelConverter.convert(user));
    }

    @Override
    public List<UserDto> findAll() {
        return userService.findAll().stream().map(userModelToUserDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    public Optional<UserDetails> getLoggedUser() {
        return userService.getLoggedUser();
    }
}
