package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.TournamentDto;
import net.aspanc.furbolin_internship.models.TournamentModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TournamentDtoToTournamentModelConverter implements Converter<TournamentDto, TournamentModel> {

    @Override
    public TournamentModel convert(TournamentDto source) {
        TournamentModel tournamentModel = new TournamentModel();
        tournamentModel.setId(source.getId());
        return tournamentModel;
    }

}
