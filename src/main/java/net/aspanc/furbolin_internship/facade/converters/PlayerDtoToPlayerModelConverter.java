package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.PlayerDto;
import net.aspanc.furbolin_internship.models.PlayerModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlayerDtoToPlayerModelConverter implements Converter<PlayerDto, PlayerModel> {

    @Override
    public PlayerModel convert(PlayerDto source) {

        PlayerModel playerModel = new PlayerModel();
        playerModel.setId(source.getId());
        playerModel.setName(source.getName());
        playerModel.setRole(source.getPlayerRole());

        return playerModel;
    }
}
