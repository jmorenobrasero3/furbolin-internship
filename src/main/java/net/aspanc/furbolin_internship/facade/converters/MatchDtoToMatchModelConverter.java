package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.MatchDto;
import net.aspanc.furbolin_internship.models.MatchModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MatchDtoToMatchModelConverter implements Converter<MatchDto, MatchModel> {

    @Override
    public MatchModel convert(MatchDto source) {
        MatchModel matchModel = new MatchModel();
        matchModel.setId(source.getId());
        matchModel.setType(source.getType());
        return matchModel;
    }
}
