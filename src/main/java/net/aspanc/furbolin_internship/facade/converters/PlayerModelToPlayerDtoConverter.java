package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.PlayerDto;
import net.aspanc.furbolin_internship.models.PlayerModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlayerModelToPlayerDtoConverter implements Converter<PlayerModel, PlayerDto> {

    @Override
    public PlayerDto convert(PlayerModel playerModel) {

        PlayerDto playerDto = new PlayerDto();
        playerDto.setId(playerModel.getId());
        playerDto.setName(playerModel.getName());
        playerDto.setPlayerRole(playerModel.getRole());
        return playerDto;

    }

}
