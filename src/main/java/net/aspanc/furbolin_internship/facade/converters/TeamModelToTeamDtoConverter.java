package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.TeamDto;
import net.aspanc.furbolin_internship.models.TeamModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TeamModelToTeamDtoConverter implements Converter<TeamModel, TeamDto> {

    @Override
    public TeamDto convert(TeamModel source) {
        TeamDto teamDto = new TeamDto();
        teamDto.setId(source.getId());
        teamDto.setName(source.getName());
        return teamDto;
    }
}
