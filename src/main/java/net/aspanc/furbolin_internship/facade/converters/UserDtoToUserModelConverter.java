package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.UserDto;
import net.aspanc.furbolin_internship.models.UserModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDtoToUserModelConverter implements Converter<UserDto, UserModel> {

    @Override
    public UserModel convert(UserDto source) {
        UserModel userModel = new UserModel();
        userModel.setUsername(source.getUsername());
        userModel.setRole(source.getRole());
        return userModel;
    }
}
