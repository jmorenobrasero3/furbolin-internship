package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.TournamentDto;
import net.aspanc.furbolin_internship.models.TournamentModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TournamentModelToTournamentDtoConverter implements Converter<TournamentModel, TournamentDto> {

    @Override
    public TournamentDto convert(TournamentModel source) {
        TournamentDto tournamentDto = new TournamentDto();
        tournamentDto.setId(source.getId());
        return tournamentDto;
    }

}
