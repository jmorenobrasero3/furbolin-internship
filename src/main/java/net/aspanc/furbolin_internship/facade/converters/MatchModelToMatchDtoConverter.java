package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.MatchDto;
import net.aspanc.furbolin_internship.models.MatchModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MatchModelToMatchDtoConverter implements Converter<MatchModel, MatchDto> {

    @Override
    public MatchDto convert(MatchModel source) {
        MatchDto matchDto = new MatchDto();
        matchDto.setId(source.getId());
        matchDto.setType(source.getType());
        return matchDto;
    }
}
