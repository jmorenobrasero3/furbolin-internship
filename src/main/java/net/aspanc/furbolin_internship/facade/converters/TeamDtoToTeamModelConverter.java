package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.TeamDto;
import net.aspanc.furbolin_internship.models.TeamModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TeamDtoToTeamModelConverter implements Converter<TeamDto, TeamModel> {

    @Override
    public TeamModel convert(TeamDto source) {
        TeamModel teamModel = new TeamModel();
        teamModel.setId(source.getId());
        teamModel.setName(source.getName());
        return teamModel;
    }
}
