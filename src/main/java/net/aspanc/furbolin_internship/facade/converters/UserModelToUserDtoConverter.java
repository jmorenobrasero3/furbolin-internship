package net.aspanc.furbolin_internship.facade.converters;

import net.aspanc.furbolin_internship.dtos.UserDto;
import net.aspanc.furbolin_internship.models.UserModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserModelToUserDtoConverter implements Converter<UserModel, UserDto> {

    @Override
    public UserDto convert(UserModel source) {
        UserDto userDto = new UserDto();
        userDto.setUsername(source.getUsername());
        userDto.setRole(source.getRole());
        return userDto;
    }
}
