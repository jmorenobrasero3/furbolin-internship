package net.aspanc.furbolin_internship.facade;

import net.aspanc.furbolin_internship.dtos.TournamentDto;
import java.util.List;
import java.util.Optional;

public interface TournamentFacade {

    List<TournamentDto> findAll();

    Optional<TournamentDto> findById(Long id);

    void delete(Long id);

    TournamentDto save(TournamentDto tournamentDto);

}
