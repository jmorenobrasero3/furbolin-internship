package net.aspanc.furbolin_internship.facade;

import net.aspanc.furbolin_internship.dtos.MatchDto;

import java.util.List;
import java.util.Optional;

public interface MatchFacade {

    Optional<MatchDto> findById(long id);

    List<MatchDto> findAll();

    void delete(long id);

    MatchDto save(MatchDto matchModel);
}
