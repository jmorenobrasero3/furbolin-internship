package net.aspanc.furbolin_internship.facade;

import net.aspanc.furbolin_internship.dtos.PlayerDto;

import java.util.List;
import java.util.Optional;

public interface PlayerFacade {

    List<PlayerDto> findAll();

    Optional<PlayerDto> findById(long id);

    List<PlayerDto> findByName(String name);

    void delete(long id);

    PlayerDto save(PlayerDto playerDto);


}
