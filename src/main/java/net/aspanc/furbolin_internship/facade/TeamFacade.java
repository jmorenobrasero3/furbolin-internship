package net.aspanc.furbolin_internship.facade;

import net.aspanc.furbolin_internship.dtos.TeamDto;

import java.util.List;
import java.util.Optional;

public interface TeamFacade {

    Optional<TeamDto> findById(long id);

    List<TeamDto> findAll();

    TeamDto save(TeamDto teamDto);

    void delete(long id);

    List<TeamDto> findByName(String name);
}
