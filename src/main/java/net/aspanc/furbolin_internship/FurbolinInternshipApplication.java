package net.aspanc.furbolin_internship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FurbolinInternshipApplication {

    public static void main(String[] args) {
        SpringApplication.run(FurbolinInternshipApplication.class, args);
    }

}
