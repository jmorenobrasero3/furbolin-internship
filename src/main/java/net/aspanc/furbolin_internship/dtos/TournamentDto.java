package net.aspanc.furbolin_internship.dtos;

import lombok.Data;

import java.util.List;

@Data
public class TournamentDto {

    private Long id;
    private List<MatchDto> matches;
    private List<TeamDto> teams;
}
