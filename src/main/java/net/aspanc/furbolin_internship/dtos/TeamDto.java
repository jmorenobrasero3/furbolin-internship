package net.aspanc.furbolin_internship.dtos;

import lombok.Data;

import java.util.List;

@Data
public class TeamDto {

    private long id;

    private String name;

    private List<MatchDto> matches;

    private TournamentDto tournament;

    private List<PlayerDto> players;
}
