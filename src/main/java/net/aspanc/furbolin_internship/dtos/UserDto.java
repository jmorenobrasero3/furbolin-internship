package net.aspanc.furbolin_internship.dtos;

import lombok.Data;
import net.aspanc.furbolin_internship.utils.enums.UserRole;

@Data
public class UserDto {
    private String username;

    private UserRole role;
}
