package net.aspanc.furbolin_internship.dtos;

import lombok.Data;
import net.aspanc.furbolin_internship.models.TeamModel;
import net.aspanc.furbolin_internship.utils.enums.PlayerRole;

import java.util.List;

@Data
public class PlayerDto {
    private long id;
    private String name;
    private PlayerRole playerRole;
    private List<TeamDto>teamDtoList;
}
