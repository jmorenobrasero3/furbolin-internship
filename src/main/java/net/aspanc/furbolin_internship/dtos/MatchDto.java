package net.aspanc.furbolin_internship.dtos;

import lombok.Data;
import net.aspanc.furbolin_internship.models.TeamModel;
import net.aspanc.furbolin_internship.models.TournamentModel;
import net.aspanc.furbolin_internship.utils.enums.MatchType;

import java.util.List;
import java.util.Map;

@Data
public class MatchDto {

    private Long id;

    private MatchType type;

    private TeamDto winner;

    private TournamentDto tournament;

    private Map<TeamDto, Integer> scores;

}
